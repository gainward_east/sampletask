package com.gainwardeast.combiner_sample_task;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class GridViewAdapter extends ArrayAdapter <String> {
	private Context context;
	private ArrayList<String> data;
	private int layoutResourceId;
	private HashMap<Integer, ImageView> imageViews = new HashMap<Integer, ImageView> ();
	private HashMap<Integer, ProgressBar> progressBars = new HashMap<Integer, ProgressBar> ();
	private HashMap<Integer, Bitmap> imagesCash = new HashMap<Integer, Bitmap>();
	private HashMap<Integer, MyAbortableRunnable> asynctaskList = new HashMap<Integer, MyAbortableRunnable>();
	public GridViewAdapter(Context context,int layoutResourceId,ArrayList<String> data) 
	{
		super(context, layoutResourceId, data);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.data = data;		
	}

	static class ItemHolder {
		ImageView icon;
		ProgressBar loadingProgressBar;
	}

	public void recycleAllImages() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < imagesCash.size();) {
					if (imagesCash.get(i) != null) {
						imagesCash.get(i).recycle();
						imagesCash.remove(i);
					}
					if (asynctaskList.get(i) != null)
						asynctaskList.get(i).abort();
					i++;
				}
			}
		}).start();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ItemHolder holder;
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		convertView = inflater.inflate(layoutResourceId, parent, false);
		holder = new ItemHolder();
		holder.icon = (ImageView) convertView.findViewById(R.id.gridview_item_image);
		holder.loadingProgressBar = (ProgressBar) convertView.findViewById(R.id.gridview_item_image_loadingprogressbar);
		String image_url = data.get(position);
		System.out.println("position " + position);
		if (imagesCash.get(position) == null) {
			System.out.println("position " + position + " load it");
			imageViews.put(position, holder.icon);
			progressBars.put(position, holder.loadingProgressBar);
			Bundle params = new Bundle();
			params.putString("image_url", image_url);
			params.putInt("position", position);
			MyAbortableRunnable asyncTask = new MyAbortableRunnable(params);
			new Thread(asyncTask).start();
			asynctaskList.put(position, asyncTask);
		} else {
			System.out.println("position " + position + " take from cash");
			holder.icon.setImageBitmap(imagesCash.get(position));
			holder.loadingProgressBar.setVisibility(View.GONE);
			holder.icon.setVisibility(View.VISIBLE);
		}
		convertView.setTag(holder);
		return convertView;
	}
	
	public class MyAbortableRunnable implements Runnable {
		private final Object lock = new Object();
		private Bundle params;
		private Bitmap target_bitmap = null;
		private int position;
		InputStream stream;
		HttpGet httpget;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse request;

		Handler handler = new Handler();
		Runnable UiRunnable = new Runnable() {
			@Override
			public void run() {
				ImageView loadedImageView = imageViews.get(position);
				if (target_bitmap != null) {
					target_bitmap = Bitmap.createScaledBitmap(target_bitmap, 140, 140, true);
					imagesCash.put(position, target_bitmap);
					loadedImageView.setImageBitmap(imagesCash.get(position));
					ProgressBar loadedProgressBar = progressBars.get(position);
					loadedProgressBar.setVisibility(View.GONE);
					loadedImageView.setVisibility(View.VISIBLE);
				} else {
					target_bitmap = BitmapFactory.decodeResource(((Activity) context).getResources(), R.drawable.ic_launcher);
					target_bitmap = Bitmap.createScaledBitmap(target_bitmap, 140, 140, true);
					imagesCash.put(position, target_bitmap);
					loadedImageView.setImageBitmap(imagesCash.get(position));
					ProgressBar loadedProgressBar = progressBars.get(position);
					loadedProgressBar.setVisibility(View.GONE);
					loadedImageView.setVisibility(View.VISIBLE);
				}
				System.out.println("AsyncTask is finished " + position);
			}
		};

		public MyAbortableRunnable(Bundle params) {
			this.params = params;
		}

		public void abort() {
			synchronized (lock) {
				if (request != null) {
					try {
						httpget.abort();
						request.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		@Override
		public void run() {
			synchronized (lock) {
				String target_url = params.getString("image_url");
				position = params.getInt("position");
				URL UrlImage;
				try {
					httpget = new HttpGet(target_url);
					request = httpClient.execute(httpget);
					HttpEntity entity = request.getEntity();
					stream = entity.getContent();
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.outWidth = 200;
					options.outHeight = 200;
					target_bitmap = BitmapFactory.decodeStream(stream, null, options); // .decodeStream(stream);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
				} finally {
					try {
						if (request != null)
							request.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					handler.post(UiRunnable);
				}
			}
		}
	}
}
