package com.gainwardeast.combiner_sample_task;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class NotificationOnClickReciever extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		
		MusicService service = ((MusicService.MusicServiceBinder) peekService(context, new Intent(context, MusicService.class))).getServiceBinder(); 
		
		if(intent.getAction().equals("com.gainwardeast.combiner_sample_task.stop_service"))
		{
			service.stopSelf();
		}
	}

}