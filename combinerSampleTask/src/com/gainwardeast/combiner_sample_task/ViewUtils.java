package com.gainwardeast.combiner_sample_task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

public class ViewUtils {
	private Context context;
	private ArrayList<String> imagesUrlList = new ArrayList <String> (){};
	private int pagesResultCount;
	private GridViewAdapter adapter;
	private GridView gridview;
	
	private Object mainNetThreadLock = new Object();
	
	public ViewUtils(Context context1) {
		this.context = context1;
		Button button_button_do_search = (Button) ((Activity) context).findViewById(R.id.button_do_search);
		button_button_do_search.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				EditText queryField = (EditText) ((Activity) context).findViewById(R.id.web_query_field);
				String queryFieldText = queryField.getText().toString();
				if (queryFieldText.equals("")) {
					showAlertDialog();
				} else {
					doImagesSearch(queryFieldText);
				}
			}
		});
	}

	private void doImagesSearch(final String queryText) {
		imagesUrlList.clear();
		if(adapter != null){adapter.recycleAllImages();adapter = null;}
		gridview = (GridView)((Activity) context).findViewById(R.id.web_query_gridview);
		gridview.setAdapter(null);
		final Handler handler = new Handler();
		final Runnable serviceForbiddenRunnable = new Runnable()
		{
			@Override
			public void run() {
				Toast.makeText(context, "Service seems to be deprecated. Try again.", Toast.LENGTH_SHORT).show();
			}
		};
		final Runnable runnable = new Runnable()
		{
			@Override
			public void run() {	
				System.out.println("size before "+imagesUrlList.size());
				for(int i=0;i<imagesUrlList.size();)
				{
					int coincidedIndex = imagesUrlList.indexOf(imagesUrlList.get(i));
					if(coincidedIndex != -1)
					{
						System.out.println("is removed "+imagesUrlList.get(coincidedIndex));
						imagesUrlList.remove(coincidedIndex);
					}
					i++;
				}
				imagesUrlList.trimToSize();
				System.out.println("size after "+imagesUrlList.size());
				adapter = new GridViewAdapter(context,R.layout.gridview_item,imagesUrlList);
				gridview = (GridView)((Activity) context).findViewById(R.id.web_query_gridview);
				gridview.setAdapter(adapter);
			}
		};
		new Thread(new Runnable() {
			@Override
			public void run() {
				String address = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=";
				String query = queryText;
				String charset = "UTF-8";
				URL url = null;
				String str = "";
				String all_strings = "";
				BufferedReader in = null;
				try {
					url = new URL(address + URLEncoder.encode(query, charset));
					in = new BufferedReader(new InputStreamReader(url.openStream()));
					while ((str = in.readLine()) != null) 
					{
						all_strings += str;
					}
					in.close();
					System.out.println("strings  "+all_strings);
				    JSONObject alldataJsonObj =null ;
			    	try {
			    		JSONParser parser = new JSONParser();
			    	Object obj;
			    	obj = parser.parse(all_strings);
			    	alldataJsonObj = (JSONObject)obj;
			    	} catch (ParseException e) {     
			    		e.printStackTrace();
			    	} 
			    	JSONObject responseDataObject = (JSONObject) alldataJsonObj.get("responseData");
			    	int serviceStatus = Integer.parseInt(alldataJsonObj.get("responseStatus").toString());
			    	if(serviceStatus == 200)
			    	{
				    	JSONArray resultsDataObject = (JSONArray) responseDataObject.get("results");
				    	JSONObject cursorDataObject = (JSONObject) responseDataObject.get("cursor");
				    	JSONArray cursorPagesDataObject = (JSONArray) cursorDataObject.get("pages");
				    	pagesResultCount = cursorPagesDataObject.size();
						for (int i=0;i<resultsDataObject.size();i++)
						{
							JSONObject jsonobject = (JSONObject) resultsDataObject.get( i );
							String target_url = jsonobject.get("url").toString();
							imagesUrlList.add(target_url);
							System.out.println("target_url  "+ target_url);
						}
						
						synchronized(mainNetThreadLock)
						{
							for (int i=0;i<cursorPagesDataObject.size();)
							{
								JSONObject jsonobject = (JSONObject) cursorPagesDataObject.get(i);
								int startWith = Integer.parseInt(jsonobject.get("start").toString());
								getImagesUrlFromPage(queryText,startWith);
								mainNetThreadLock.wait();
								i++;
							}
						}
			    	}else
			    	{
			    		handler.post(serviceForbiddenRunnable);
			    	}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.post(runnable);
			}
		}).start();
	}

	private void getImagesUrlFromPage(final String queryText,final int page) {

		new Thread(new Runnable() {
			@Override
			public void run() {
				String address = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&start="+page+"&q=";
				String query = queryText;
				String charset = "UTF-8";
				URL url = null;
				String str = "";
				String all_strings = "";
				BufferedReader in = null;
				try {
					url = new URL(address + URLEncoder.encode(query, charset));
					in = new BufferedReader(new InputStreamReader(url.openStream()));
					while ((str = in.readLine()) != null) 
					{
						all_strings += str;
					}
					in.close();
				    JSONObject alldataJsonObj =null ;
			    	try {
			    		JSONParser parser = new JSONParser();
			    	Object obj;
			    	obj = parser.parse(all_strings);
			    	alldataJsonObj = (JSONObject)obj;
			    	} catch (ParseException e) 
			    	{     
			    		e.printStackTrace();
			    	} 
			    	JSONObject responseDataObject = (JSONObject) alldataJsonObj.get("responseData");
			    	int serviceStatus = Integer.parseInt(alldataJsonObj.get("responseStatus").toString());
			    	if(serviceStatus == 200)
			    	{
				    	JSONArray resultsDataObject = (JSONArray) responseDataObject.get("results");
				    	JSONObject cursorDataObject = (JSONObject) responseDataObject.get("cursor");
				    	JSONArray cursorPagesDataObject = (JSONArray) cursorDataObject.get("pages");
				    	pagesResultCount = cursorPagesDataObject.size();
						for (int i=0;i<resultsDataObject.size();i++)
						{
							JSONObject jsonobject = (JSONObject) resultsDataObject.get( i );
							String target_url = jsonobject.get("url").toString();
							imagesUrlList.add(target_url);
						}
						System.out.println("getImagesUrlFromPage is ready for page "+page);
			    	}
				} catch (IOException e) {
					e.printStackTrace();
				}
				synchronized(mainNetThreadLock) {
					mainNetThreadLock.notify();
		        }
			}
		}).start();
	}
	
	private void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder
		.setTitle("Warning !")
		.setMessage("Have you forgot to enter a query ?")
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

}
