package com.gainwardeast.combiner_sample_task;

import android.graphics.Bitmap;

public class GridViewItem {

	Bitmap bitmap;
	
	public GridViewItem(Bitmap bitmap)
	{
		this.bitmap = bitmap;
	}
}
