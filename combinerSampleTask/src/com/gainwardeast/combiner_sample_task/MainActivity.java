package com.gainwardeast.combiner_sample_task;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	private Intent serviceIntent;
	private ServiceConnection serviceConnection;
	private MusicService backgroundMusicService;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ViewUtils renderingClassObject = new ViewUtils(this);
		serviceIntent = new Intent(this, MusicService.class);
		serviceConnection = new ServiceConnection(){
			@Override
			public void onServiceConnected(ComponentName arg0, IBinder binder) {
				System.out.println("onServiceConnected()");		
				backgroundMusicService = ((MusicService.MusicServiceBinder) binder).getServiceBinder();
			}
			@Override
			public void onServiceDisconnected(ComponentName name) {
				System.out.println("onServiceDisconnected()");			
			}};
	}
	@Override
	protected void onResume()
	{
		super.onResume();
		startService(serviceIntent);
		bindService(serviceIntent, serviceConnection, 0);
		Button pause_music_button = (Button)findViewById(R.id.pause_music_button);
		pause_music_button.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				backgroundMusicService.pauseUglyMusic();
			}});
	}
	
	@Override
	protected void onDestroy() 
	{
		unbindService(serviceConnection);
		super.onDestroy();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
