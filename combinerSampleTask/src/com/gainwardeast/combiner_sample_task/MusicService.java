package com.gainwardeast.combiner_sample_task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;

public class MusicService extends Service{

	private MusicServiceBinder binder = new MusicServiceBinder();
	private int ONGOING_NOTIFICATION_ID = 1111;
	private ArrayList <String> trackList = new ArrayList<String>(){};
	private MediaPlayer mediaPlayer = new MediaPlayer();
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		System.out.println("MusicService started");
		playSomeMusic();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		System.out.println("MusicService destroyed");
		if (mediaPlayer != null) {
			try {
				mediaPlayer.release();
				mediaPlayer = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void playSomeMusic()
	{
		// ask android media database for tracks paths
		ContentResolver cr = getContentResolver();
		Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
		String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
		Cursor cur = cr.query(uri, null, selection, null, sortOrder);
		int count = 0;
		if(cur != null)
		{
		    count = cur.getCount();
		    if(count > 0)
		    {
		        while(cur.moveToNext())
		        {
		            String data = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA));
		            System.out.println("found data "+data);
		            trackList.add(data);
		        }
		    }
		}
		cur.close();
		
		int filesListSize = trackList.size();
		if(filesListSize != 0)
		{
			Random ran = new Random();
			int targetFileInt =  ran.nextInt(filesListSize);
			try {
				System.out.println("selected "+trackList.get(targetFileInt));
				mediaPlayer.setDataSource(trackList.get(targetFileInt));
				mediaPlayer.prepare();
				mediaPlayer.start();
				set_tray_notication(new File(trackList.get(targetFileInt).toString()).getName());	
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("Couldnt find any files");
		}
	}
	
	public void pauseUglyMusic()
	{
		mediaPlayer.pause();
	}
	
	public void set_tray_notication(String fileName) 
	{
		Notification notification = new Notification(R.drawable.ic_launcher, "Music Service", System.currentTimeMillis());
		// notification on press intent
		Intent notificationIntent = new Intent(this, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		// cancel button intent
		Intent cancelButtonIntent = new Intent("com.gainwardeast.combiner_sample_task.stop_service");
		PendingIntent cancelButtonIntentPendingintent = PendingIntent.getBroadcast(this, 0, cancelButtonIntent, 0);
		// notification style
		NotificationCompat.BigTextStyle notiStyle = new NotificationCompat.BigTextStyle();
		notification = new NotificationCompat.Builder(this)
		.setSmallIcon(R.drawable.ic_launcher)
		.setAutoCancel(true)
		.addAction(0, "Stop service", cancelButtonIntentPendingintent)
		.setContentIntent(pendingIntent)
		.setContentTitle("Music Service")
		.setSubText("Now playing -> "+fileName)
		.setContentText("Now playing -> "+fileName)
		.setStyle(notiStyle).build();
		startForeground(ONGOING_NOTIFICATION_ID, notification);
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {
		
		return binder;
	}
	public class MusicServiceBinder extends Binder
	{
		public MusicService getServiceBinder()
		{
			return MusicService.this;
		}
	}
}
